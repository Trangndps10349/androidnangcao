package com.example.ps10349_lap3;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class AdapterGvMedia extends BaseAdapter {
    Context context;
    ArrayList <Uri> media;

    public AdapterGvMedia(Context context, ArrayList<Uri> media) {
        this.context = context;
        this.media = media;
    }

    @Override
    public int getCount() {
        return media.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    class ViewHolder{
       ImageView imageView;

    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view==null){
            holder= new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(R.layout.item_gv_media,null);
            holder.imageView=view.findViewById(R.id.image_item);
            view.setTag(holder);
        }else{
        holder = (ViewHolder) view.getTag();
    }
    Uri UriHinh = media.get(i);
        holder.imageView.setImageURI(UriHinh);
        return view;

    }
}
